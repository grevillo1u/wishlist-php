Wishlist - Vous accompagne, vous et vos amis !
========

### Des amis en manque d'inspiration ? Vous voulez tout simplifier ? Wishlist est fait pour vous ! 

##### Utiliser notre site web 100% fonctionnel
* Copier le lien suivant dans la barre d'URL de votre navigateur :
    `https://wishlist.sytes.net`

##### Instructions d'installation (sur son serveur web) :
* Clôner le dépôt dans le dossier lu par votre serveur web
* Ajouter un fichier `.htaccess` à la racine du projet autorisant la réécriture d'url sur le fichier index.php et n'autorisant pas l'accès aux fichiers sources
 Exemple :

        RewriteRule ^src(/.*|)$ - [NC,F]
        RewriteRule ^vendor(/.*|)$ - [NC,F]
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ index.php [QSA,L]
        
* Ajouter un fichier conf.ini contenant les identifiants qui vous ont été fournis et placer le fichier dans src/conf (à créer) : 

        driver=mysql
        database=wishlist
        username=
        password=
        host=wishlist.sytes.net


* Lancer `composer install` à la source du projet afin d'installer toutes les dépendances.
* Enjoy ! Vous n'avez plus qu'à lancer votre navigateur web et de profiter de notre site web !
