<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 19/11/18
 * Time: 15:30
 */
namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model {
    protected $table = "liste";
    protected $primaryKey = 'no';
    public $timestamps = false;

    public function items() {
        return $this->hasMany('\mywishlist\models\Item', 'liste_id');
    }

    public function compte() {
        return $this->belongsTo("\mywishlist\models\Compte", "idCompte");
    }
}