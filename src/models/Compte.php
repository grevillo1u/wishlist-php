<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/12/18
 * Time: 15:36
 */

namespace mywishlist\models;

use \Illuminate\Database\Eloquent\Model as Model;

class Compte extends Model {
    protected $table = 'Compte';
    protected $primaryKey = 'idCompte';
    public $timestamps = false;

    public function listes() {
        return $this->hasMany("\mywishlist\models\Liste", "idCompte");
    }

   /* public function __construct($nom, $listes) {
        $this->nom = $nom;
        $this->listesPerso = $listes;
    }

    public function get($attr) {
        if(property_exists($this, $attr))
            return $this->$attr;
        else
            throw new \Exception("$attr : Attribut inexistant");
    }

    public function set($attr, $valeur) {
        if(property_exists($this, $attr)) {
            $this->$attr = $valeur;
            return $this->$attr;
        }
        else
            throw new \Exception("$attr : Attribut Inexistant");
    }*/
}