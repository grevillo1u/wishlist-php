<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/12/18
 * Time: 16:32
 */

namespace mywishlist\controllers;

use \mywishlist\models\Item as Item;
use \mywishlist\models\Liste as Liste;
use \mywishlist\models\Compte as Compte;
use \mywishlist\controllers\ControleAuthentification as Authentification;
use \mywishlist\vue\VueCreateur as VueCreateur;

/**
 * Class ControleCreateur
 * Regroupe les methodes utiles pour la personne creant une liste
 * @package mywishlist\controllers
 */
class ControleCreateur extends Controller {

    private $rootUri;

    public function __construct() {
        parent::__construct();
        $app = \Slim\Slim::getInstance();
        $this->rootUri = $app->request->getRootUri();
    }

    /**
     * Permet a un utilisateur connecte de creer une liste de cadeau
     * @param $tableauElements
     *      tableau des valeurs a renseigne
     */
    public function creerListe() {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                $liste = new Liste();
                $liste->user_id = filter_var($_POST['user_id'], FILTER_SANITIZE_NUMBER_INT);//$_SESSION["profile"]['idUser'];
                $liste->titre = filter_var($_POST['titre'], FILTER_SANITIZE_STRING);
                $liste->description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
                $liste->expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                $liste->save();

                //Création de l'affichage
                $vc = new VueCreateur(VueCreateur::LISTE_CREE, null);
                $vc->render();
            }
        }
    }

    /**
     * Permet au createur de la liste de pouvoir creer un cadeau dans sa liste
     * @param $tabItem
     *      Cadeau a cree
     */
    public function ajouterItemDansListe($idListe) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                $item = new Item();

                $item->liste_id = filter_var($idListe, FILTER_SANITIZE_NUMBER_INT);
                $item->nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
                $item->descr = filter_var($_POST['descr'], FILTER_SANITIZE_STRING);
                $item2 = $this->uploadImage($item, null);
                if(!$item2)
                    echo "<script>alert('Malfonctionnement dans l\'upload de l\'image');</script>";
                else
                    $item = $item2;
                $item->tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT);

                $item->save();

                $vc = new VueCreateur(VueCreateur::AJOUT_ITEM, null);
                echo $vc->render();
            }
        } else if(isset($_POST["tokenCreateur"]) && !is_null($content = Liste::where('tokenCreateur', '=', $idListe)->first())) {
            $item = new Item();

            $id = Liste::where("tokenCreateur", $idListe)->first()->no;

            $item->liste_id = $id;
            $item->nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
            $item->descr = filter_var($_POST['descr'], FILTER_SANITIZE_STRING);
            $item2 = $this->uploadImage($item, null);
            if(!$item2)
                echo "<script>alert('Malfonctionnement dans l\'upload de l\'image');</script>";
            else
                $item = $item2;
            $item->tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT);

            $item->save();

            $app = \Slim\Slim::getInstance();
            $app->redirectTo("viewListInvite", array("tokenCreation" => $idListe));
        } else {
            echo "Coucou";
            echo "\n<br />" . var_dump($_POST);
        }
    }

    /**
     * @param $item
     *      item contenant l'image
     * @param $nomFichier
     *      nomFichier qu'on souhaite attribuer à cette image
     * @return bool
     *      faux si l'upload ne fonctionne pas
     */
    private function uploadImage($item, $nomFichier) {
        //TODO AUTORISER UNIQUEMENT FICHIER IMAGES (PAS SEULEMENT EXTENSION)
        $listeExtensionAccept = array("jpg", "png", "gif", "jpeg");
        if(isset($_POST)) {
            if(isset($_FILES['image'])) {
                //Vérification de l'extension du fichier
                $extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                if(in_array(strtolower($extension), $listeExtensionAccept)) {
                    // Parcours du tableau d'erreurs
                    if(isset($_FILES['image']['error']) && UPLOAD_ERR_OK === $_FILES['image']['error']) {
                        //On génère un id pour le nom de l'image ou on récupère l'ancien si disponible
                        if(is_null($nomFichier))
                            $nomImage = uniqid() . "." . strtolower($extension);
                        else
                            $nomImage = $nomFichier;

                        echo $nomImage;
                        if(move_uploaded_file($_FILES['image']['tmp_name'], "images/$nomImage")) {
                            $item->img = $nomImage;
                            return $item;
                        } else {
                            echo "<script>alert('Erreur upload');</script>";
                        }
                    } else {
                        echo "<script>alert('Erreur dans le tableau du fichier');</script>";
                        var_dump($_FILES['image']);
                        echo "<br />" . UPLOAD_ERR_OK;                    }
                } else
                    echo "<script>alert('Erreur extension');</script>";
            } else
                echo "<script>alert('Erreur FILES INEXISTANT');</script>";
        } else
            echo "<script>alert('Erreur POST Inexistant');</script>";
        return false;
    }

    /**
     * Genere un lien de partage avec token permettant à n'importe qui de réserver un cadeau
     * @param $idListe
     *      Liste a partager
     * @param $idCompte
     *      Compte createur de la liste
     */
    public function partagerListe($idListe) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                //Génération du token
                $token = openssl_random_pseudo_bytes(10);
                $token = bin2hex($token);
                //Mise à jour du token dans la table
                $listeRecherche = Liste::where("user_id", $_SESSION['profile']['user_id'])->where("no", $idListe)->first();
                if (!is_null($listeRecherche)) {
                    $listeRecherche->token = $token;
                    $listeRecherche->save();
                    echo "<script>alert('Génération du token réussie !'</script>";
                }
                $v = new VueCreateur(VueCreateur::SHARE_LIST, null);
                $v->render();
            }
        } else if(!is_null($content = Liste::where('tokenCreateur', '=', $idListe)->first())) {
            //En tant qu'invité on récupère la liste
            $liste = Liste::where('tokenCreateur', $idListe)->first();
            if(!is_null($liste)) {
                //Génération du token
                $token = openssl_random_pseudo_bytes(10);
                $token = bin2hex($token);
                //Mise à jour du token dans la table
                $liste->token = $token;
                $liste->save();
            }
            $v = new VueCreateur(VueCreateur::SHARE_LIST, null);
            echo $v->render();

        }
    }

    /**
     * Recupere le contenu d'une liste
     * @param $idListe
     */
    public function recupererContenuListe($idListe) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                $content = Item::where("liste_id", "$idListe")->get();
                $vc = new VueCreateur(VueCreateur::AFFICHE_CONTENU_LISTE, $content);
                echo $vc->render();
            }
        } else if(!is_null($content = Liste::where('tokenCreateur', '=', $idListe)->first())) {
            $cadeaux = Item::where('liste_id', $content->no)->get();
            $v = new VueCreateur(VueCreateur::AFFICHE_CONTENU_LISTE, $cadeaux);
            echo $v->render();
        } else {
            $app = \Slim\Slim::getInstance();
            $app->redirectTo('Accueil');
        }
    }

    /**
     *  Permet d'afficher le formulaire de modification d'une liste
     * @param $idListe
     *      id de la liste a modifier
     */
    public function modificationListe($idListe) {
        if(isset($_SESSION['profile']))
            $content = Liste::where("no", $idListe)->first();
        else
            $content = Liste::where("tokenCreateur", $idListe)->first();

        $v = new VueCreateur(VueCreateur::FORM_MODIF_LIST, $content);
        echo $v->render();
    }

    /**
     * Renvoie le tableau des listes de l'utilisateur
     * @return mixed
     *      Tableau des listes de l'utilisateur
     */
    public function recupererListes() {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if(Authentification::checkAccessRight(0)) {
                $liste = Liste::where("user_id", "=", $_SESSION['profile']['user_id'])->orderBy('no', 'ASC')->get();
                $vc = new VueCreateur(VueCreateur::AFFICHE_LISTE, $liste);
                echo $vc->render();
                return $liste;
            } else {
                echo "<script>alert('Permission insuffisante pour accéder à cette page');</script>";
            }
        } else {
            echo "<script>alert('Veuillez-vous identifier pour continuer');</script>";
        }
    }

    /**
     * Recupere les informations sur un item
     * @param $idListe
     *      id de la liste à laquelle appartient l'item
     * @param $idItem
     *      id de l'item a recuperer
     */
    public function recupererItem($idListe, $idItem) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                //On vérifie que l'utilisateur a bien accès à la liste
                $user_id = Liste::select('user_id')->where('no', $idListe)->first();
                if($_SESSION['profile']['user_id'] === $user_id->user_id) {
                    $item = Item::where('id', $idItem)->first();
                    $v = new VueCreateur(VueCreateur::AFFICHE_ITEM, $item);
                    echo $v->render();
                }
            }
        } else {
            //On part donc en mode invité s'il n'est pas connecté
            $listeViaToken = Liste::where('tokenCreateur', $idListe)->first();
            $item = Item::where('id', $idItem)->first();
            $listeViaItem = Liste::where('no', $item->liste_id)->first();
            if($listeViaItem->no === $listeViaToken->no) {
                //On renvoie vers la vue si les listes concordent
                $v = new VueCreateur(VueCreateur::AFFICHE_ITEM, $item);
                echo $v->render();
            } else {
                //On redirige vers la page d'accueil sinon
                $app = \Slim\Slim::getInstance();
                $app->redirectTo('Accueil');
            }
        }
    }

    /**
     * Modifie un cadeau
     * @param $idListe
     *      Id de la liste
     * @param $idItem1
     *      Id du cadeau a modifier
     */
    public function setItem($idListe, $idItem1) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                //Chargement de l'item depuis la base
                if (isset($_POST)) {
                    $idItem = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
                    $item = Item::where("id", $idItem)->first();

                    if ($item !== null) {
                        $item->nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
                        $item->descr = filter_var($_POST['descr'], FILTER_SANITIZE_STRING);
                        $item->tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        $item2 = $this->uploadImage($item, $item->img);
                        if(!$item2){
                            //La modification de l'image est facultative ici donc on ne fait rien si rien n'est sélectionné
                        }
                        else
                            $item = $item2;

                        $item->save();

                        //Création de la page de redirection
                        $app = \Slim\Slim::getInstance();
                        $app->redirectTo('ViewItemCrea', array('idListe' => $idListe, 'idItem' => $idItem1));

                    } else
                        echo "Impossible de modifier : Objet introuvable";
                }
            }
        } else {
            $app = \Slim\Slim::getInstance();
            //Si l'utilisateur est un invité, on va vérifier que la liste existe pour pouvoir la modifier via le token généré
            try {
                $listeViaToken = Liste::where('tokenCreateur', $idListe)->first();
                $item = Item::where('id', filter_var($idItem1, FILTER_SANITIZE_NUMBER_INT))->first();
                $listeViaItem = Liste::where('no', $item->liste_id)->first();
            }catch(\ErrorException $e) {
                //S'il y a une erreur quelconque on renvoie vers l'accueil immédiatement
                $app->redirectTo("Accueil");
            }
            //On vérifie que l'id des 2 listes concordent
            if($listeViaToken->no === $listeViaItem->no) {
                if ($item !== null) {
                    $item->nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
                    $item->descr = filter_var($_POST['descr'], FILTER_SANITIZE_STRING);
                    $item->tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $item2 = $this->uploadImage($item, $item->img);
                    if (!$item2) {
                        //La modification de l'image est facultative ici donc on ne fait rien si rien n'est sélectionné
                    } else
                        $item = $item2;

                    $item->save();

                    //Création de la page de redirection
                    $app = \Slim\Slim::getInstance();
                    $app->redirectTo('viewItemInvite', array('tokenCreateur' => $idListe, 'idItem' => $idItem1));
                }
            }
        }
    }

    /**
     * Supprime un cadeau
     * @param $idItem
     *      Cadeau a supprime
     */
    public function deleteItem($idListe, $idItem) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                //On vérifie que l'utilisateur est bien le créateur de l'item en question
                if($this->estLeCreateurItem($idItem)) {
                    $item = Item::where("id", '=', $idItem)->first();
                    if (!is_null($item)) {
                        //Suppression de l'image sur le serveur
                        $nomFichier = "images/" . $item->img;
                        try {
                            unlink($nomFichier);
                        }catch(\ErrorException $e){}
                        $item->delete();
                    }
                }
            }
        } else {
            $app = \Slim\Slim::getInstance();
            //Suppression d'un item dans le cas d'un utilisateur invité
            //On vérifie que le token correspond bien à la liste voulue
            try {
                $listeViaToken = Liste::where('tokenCreateur', $idListe)->first();
                $item = Item::where('id', filter_var($idItem, FILTER_SANITIZE_NUMBER_INT))->first();
                $listeViaItem = Liste::where('no', $item->liste_id)->first();

            }catch(\ErrorException $e) {
                //S'il y a une erreur quelconque on renvoie vers l'accueil immédiatement
                $app->redirectTo("Accueil");
            }

            if($listeViaItem->no === $listeViaToken->no) {
                if (!is_null($item)) {
                    //Suppression de l'image sur le serveur
                    $nomFichier = "images/" . $item->img;
                    try {
                        unlink($nomFichier);
                    }catch(\ErrorException $e){}
                    $item->delete();
                }
            }
        }
    }

    /**
     * Permet de modifier une liste
     * @param $idListe
     *      id de la liste à modifier
     */
    public function setListe($idListe) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                //On vérifie que l'utilisateur est bien le créateur de la liste en question
                if($this->estLeCreateurListe($idListe)) {
                    //Chargement de la liste depuis la base
                    if (isset($_POST)) {
                        $liste = Liste::where("no", $idListe)->first();

                        if ($liste !== null) {
                            $liste->titre = filter_var($_POST['titre'], FILTER_SANITIZE_STRING);
                            $liste->description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
                            $liste->expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_STRING);
                            $liste->save();
                        }
                    }
                }
                //Création de la page de redirection
                $app = \Slim\Slim::getInstance();
                $app->redirectTo('viewOneListe', array('idListe' => $idListe));
            }
        } else if(!is_null(Liste::where('tokenCreateur', $idListe)->first())) {
            //Modification d'une liste en étant invité
            $app = \Slim\Slim::getInstance();
            if(isset($_POST)) {
                $liste = Liste::where("tokenCreateur", $idListe)->first();
                $liste->titre = filter_var($_POST['titre'], FILTER_SANITIZE_STRING);
                $liste->description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
                $liste->expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_STRING);
                $liste->save();
            }
            $app->redirectTo("viewListInvite", array("tokenCreation" => $idListe));
        }
    }

    /**
     * Permet la suppression d'une liste
     * @param $idListe
     *  liste à supprimer
     */
    public function deleteListe($idListe) {
        //Si l'utilisateur est connecté
        if(isset($_SESSION['profile'])) {
            //Si l'utilisateur connecté a les droits pour consulter la page
            if (Authentification::checkAccessRight(0)) {
                //On vérifie que l'utilisateur est bien le créateur de la liste en question
                if($this->estLeCreateurListe($idListe)) {
                    //On supprime les items de la liste avant de supprimer la liste de la base de données
                    $listeItem = Item::where('liste_id', $idListe)->get();
                    foreach($listeItem as $value)
                        $this->deleteItem($idListe, $value->id);

                    //On supprime la liste maintenant
                    $liste = Liste::where('no', $idListe)->first();
                    if(!is_null($liste))
                        $liste->delete();
                }
                $app = \Slim\Slim::getInstance();
                $app->redirectTo("viewListes");
            }
        } else if(!is_null(Liste::where('tokenCreateur', $idListe)->first())) {
            echo "Bien";
            //On supprime les items de la liste avant de supprimer la liste de la base de don;nées
            $listeReel = Liste::where('tokenCreateur', $idListe)->first();
            $listeItem = Item::where('liste_id', $listeReel->no)->get();

            foreach($listeItem as $value)
                $this->deleteItem($idListe, $value->id);

            //On supprime la liste maintenant
                $listeReel->delete();
                print_r($listeReel);
            $app = \Slim\Slim::getInstance();
            $app->redirectTo("Accueil");
        } else {
            echo "Perdu";
        }
    }

    /**
     * Crée une liste pour un invité
     */
    public function createListInvite() {
        if(isset($_POST)) {
            $liste = new Liste();
            $liste->titre = filter_var($_POST['titre'], FILTER_SANITIZE_STRING);
            $liste->description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
            $liste->expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

            //Génération du token
            $token = openssl_random_pseudo_bytes(10);
            $token = bin2hex($token);
            $liste->tokenCreateur = $token;
            $liste->save();

            //Redirection vers cette liste
            $app = \Slim\Slim::getInstance();
            $app->redirectTo("viewListInvite", array("tokenCreation" => $token));
        }

    }

    /**
     * Verifie que l'utilisateur connecté est bien le propriétaire de l'item
     * @param $idItem
     *      id de l'item a verifier
     * @return bool
     *      vrai si l'utilisateur est le propriétaire
     */
    private function estLeCreateurItem($idItem) {
        $liste = Item::where('id', $idItem)->first()->liste()->first();
        $utilisateur = $liste->user_id;

        if($utilisateur === $_SESSION['profile']['user_id'])
            return true;
        return false;
    }

    /**
     * Verifie que l'utilisateur connecte est bien le propriétaire de la liste
     * @param $idListe
     *      id de la liste a verifier
     * @return bool
     *      vrai si l'utilisateur est le propriétaire
     */
    private function estLeCreateurListe($idListe) {
        $liste = Liste::where('no', $idListe)->first();

        if($liste->user_id === $_SESSION['profile']['user_id'])
            return true;
        return false;
    }
}