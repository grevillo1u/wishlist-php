<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 10/12/18
 * Time: 11:42
 */

namespace mywishlist\controllers;


use mywishlist\models\Item as Item;
use mywishlist\models\Liste as Liste;
use mywishlist\vue\VueParticipant as VueParticipant;

/**
 * Regroupe les methodes utiles pour la personne souhaitant reserver ou interagir avec la liste de c
 * adeaux
 * @package mywishlist\controllers
 */
class ControleParticipant extends Controller {

    /**
     * Affiche les cadeaux de la liste
     * @param $token
     *  token d'autorisation d'acces a une liste
     */
    public function afficherListeByToken($token) {
        $idListe = Liste::where("token", $token)->first()->no;
        $content = Item::where("liste_id", $idListe)->get();
        $_SESSION['token'] = $token;

        $vp = new VueParticipant($content, VueParticipant::ITEMS_VIEW);
        echo $vp->render();
    }

    /**
     * Permet a un utilisateur non connecte mais ayant acces au token de reserver un item
     * @param $idItem
     * @param $nom
     */
    public function reserverItem($token, $idItem) {
        if(isset($_POST['pseudoReservation'])) {
            $idListe = Liste::where("token", "=", $token);

            $itemRecherche = Item::where("id", $idItem)->first();
            $itemRecherche->pseudoReservation = filter_var($_POST['pseudoReservation'], FILTER_SANITIZE_STRING);
            $itemRecherche->messageReserv = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
            $itemRecherche->save();

            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('viewItem', array("token" => $token, "idItem" => $idItem)));
        }
    }

    /**
     * Obtiens la liste des items d'une liste
     * @param $token
     *      Token d'accès
     * @param $idItem
     *      Item à chercher
     */
    public function afficherItem($token, $idItem) {
        $idList = Liste::where('token', '=', $token)->first()->no;
        $item = Item::where("liste_id", '=', $idList)->where('id', '=', $idItem)->first();

        $v = new VueParticipant($item, VueParticipant::ITEM_VIEW);
        echo $v->render();
    }

    //==============================================================================================================
    //Fonctions non utilisées dans le projet mais utilisé dans le TD.
    public function getAllItems() {
        $content = array();
        foreach(Item::all() as $value)
            $content[] = $value;
        $vp = new VueParticipant($content, VueParticipant::ITEMS_VIEW);
        echo $vp->render();
    }

    public function getAllListes() {
        $content = array();
        foreach(Liste::all() as $values)
            $content[] = $values;
        $vp = new VueParticipant($content, VueParticipant::LIST_VIEW);
        echo $vp->render();
    }

    public function getItemRecherche($id) {
            $content = Item::where("id", "=", $id)->first();
        $vp = new VueParticipant($content, VueParticipant::ITEM_VIEW);
        echo $vp->render();
    }
}