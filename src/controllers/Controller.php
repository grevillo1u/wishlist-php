<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 15/12/18
 * Time: 11:19
 */

namespace mywishlist\controllers;

/**
 * Class Controller
 * @package mywishlist\controllers
 *  Permet l'activation de session sur l'ensemble des pages qui vont être chargées
 */
abstract class Controller {

    /**
     * Controller constructor.
     *  Lance session_start();
     */
    public function __construct() {
        session_start();
    }
}