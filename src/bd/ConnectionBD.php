<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 19/11/18
 * Time: 15:47
 */

namespace mywishlist\bd;
use Illuminate\Database\Capsule\Manager as DB;

class ConnectionBD {

    public static function start($file) {
        $db = new DB();
        $db->addConnection(parse_ini_file($file));
        $db->setAsGlobal();
        $db->bootEloquent();
    }

}