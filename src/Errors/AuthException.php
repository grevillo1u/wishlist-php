<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 01/01/19
 * Time: 14:48
 */

namespace mywishlist\Errors;


use Throwable;

class AuthException extends \Exception {
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}