<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 10/12/18
 * Time: 10:46
 */

namespace mywishlist\vue;

use \mywishlist\models\Item as Item;


class VueParticipant {
    private $selecteur;
    private $items;
    private $rootUri;
    const ITEMS_VIEW = 2, ITEM_VIEW = 3;

    /**
     * VueParticipant constructor.
     * @param $items
     *      Valeur de la base de donnees
     * @param $select
     *      Selecteur de vue
     */
    public function __construct($items, $select) {
        $this->selecteur = $select;
        $this->items = $items;
        $app = \Slim\Slim::getInstance();
        $this->rootUri =  $app->request->getRootUri();
    }

    /**
     * Renvoie le code HTML de la page souhaitée
     * @return string
     *  Code HTML
     */
    public function render() {
        $content = "";
        switch($this->selecteur) {
            case self::ITEM_VIEW:
                $content = $this->afficherItemParticulier();
                break;

            case self::ITEMS_VIEW:
                $content = $this->afficherListeItem();
                break;
        }

        $page = <<<EOF
<!DOCTYPE html>
<html lang='fr'>
    <head>
        <meta charset="utf-8" />
        <title>Vue Participant</title>
        <link rel='stylesheet' href="$this->rootUri/css/bootstrap.min.css" type="text/css" />   
        <link rel='stylesheet' href="$this->rootUri/css/formulaireStyle.css" type="text/css" />  
        <link rel="stylesheet" href="$this->rootUri/css/style.css" type="text/css" />
    </head>
    <body>        
            $content
    </body>
</html>
EOF;
        return $page;

    }

    /**
     * Affiche le contenu de la liste
     * @return string
     *      Code HTML correspondant
     */
    private function afficherListeItem() {
        $titreListe = $this->items[0]->liste()->first()->titre;
        $content = <<< EOF
        <div class="jumbotron">
                <h1 class = 'display-4'>$titreListe</h1>
                <h2>Voici les cadeaux qui me feraient plaisir : \n</h2>
                <p class='lead'>\n
                <div class = 'list-group'>
EOF;
        foreach ($this->items as $value) {
            $app = \Slim\Slim::getInstance();
            $urlViewItem = $app->urlFor('viewItem', array("token" => $this->getToken($app), "idItem" => $value->id));
            $badge = "";
            if(!is_null($value->pseudoReservation))
                $badge = '<span class="badge badge-success" >Réservé</span>';

            $content = <<< EOF
                $content                
                <a href=$urlViewItem class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                     <h5 class="mb-1">$value->nom $badge</h5>
                    <small>$value->expiration</small>
                </div>
        <p class="mb-1">$value->descr</p>
        <small>$value->tarif €</small>
        </a>
EOF;
        }
        $content = $content . "\n</div>";
        return $content;
    }

    /**
     * Affiche le détail d'un itemH
     * @return string
     *      HTML Correspondant
     */
    private function afficherItemParticulier() {
            try {
                $value = $this->items;
                $app = \Slim\Slim::getInstance();
                $urlReservItem = $app->urlFor('holdItem', array("token" => $this->getToken($app), "idItem" => $this->items->id));

                if(is_null($value->pseudoReservation)) {
                    $questionnaire = <<< EOF
            <div class="form-reservation">
                <form action="$urlReservItem" method="post">
                    <label for="pseudo">Pseudo : </label>
                    <input id="pseudo" type="text" name="pseudoReservation" placeholder="Entrez votre pseudo" />
                    <label for="message">Message : </label>
                    <textarea id="message" name="message" placeholder="Entrez un message que votre ami verra après lui avoir offert." cols="50" rows="2"></textarea>
                    <button class="btn btn-primary" type="submit">Réserver</button>
                </form>
            </div>
EOF;
                } else {
                    $questionnaire = <<< EOF
            <div class="alert alert-success" role="alert">
                Ce cadeau a déjà été réservé par $value->pseudoReservation !
            </div>
EOF;
                }
                    $content = <<< EOF
    <div class="card cardItem">
        <img src="$this->rootUri/images/$value->img" alt="Image de $value->nom" class="card-img-top imgDansCard" />
        <div class="card-body">
            <h5 class="card-title">$value->nom</h5>
            <h6 class="card-subtitle">Prix : $value->tarif €</h6>
            <p class="card-text">$value->descr</p>
            $questionnaire
        </div>
    </div>
EOF;
            }catch(\ErrorException $e) {
                $content = "Aucun élément n'a été trouvé !";
            }
        return $content;
    }
    /**
     * Obtiens le token en fin d'URL
     * @param $app
     *      Instance de slim
     * @return mixed
     *      Token souhaitée
     */
    private function getToken($app) {
        $urlActu = $app->request->getResourceUri();
        $tab = explode("/", $urlActu);
        return filter_var($tab[3], FILTER_SANITIZE_SPECIAL_CHARS);
    }
}